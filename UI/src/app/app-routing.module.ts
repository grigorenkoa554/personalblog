import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './account/login/login.component';
import { LogoutComponent } from './account/logout/logout.component';
import { RegisterComponent } from './account/register/register.component';
import { BlogDetailsComponent } from './blog/blog-details/blog-details.component';
import { MyBlogsComponent } from './blog/my-blogs/my-blogs.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  { path: '', component: WelcomeComponent },
  { path: 'my-blogs', component: MyBlogsComponent },
  { path: 'blog-details/:blogId', component: BlogDetailsComponent },
  { path: 'account/login', component: LoginComponent },
  { path: 'account/logout', component: LogoutComponent },
  { path: 'account/register', component: RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
