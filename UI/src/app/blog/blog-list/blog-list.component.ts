import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../account/account.service';
import { Blog } from '../blog.model';
import { BlogService } from '../blog.service';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.less']
})
export class BlogListComponent implements OnInit {

  items: Blog[] = [];

  constructor(private blogService: BlogService, public accountService: AccountService) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.blogService.getBlogs().subscribe((data) => {
      this.items = data;
    });
  }

}
