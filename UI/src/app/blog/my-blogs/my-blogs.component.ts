import { Component, OnInit } from '@angular/core';
import { Guid } from 'guid-typescript';
import { Blog } from '../blog.model';
import { BlogService } from '../blog.service';

@Component({
  selector: 'app-my-blogs',
  templateUrl: './my-blogs.component.html',
  styleUrls: ['./my-blogs.component.less']
})
export class MyBlogsComponent implements OnInit {

  isVisible = false;
  suggestions = ['afc163', 'benjycui', 'yiminghe', 'RaoHai', '中文', 'にほんご'];
  newTitle: string;
  newText?: string;
  items: Blog[] = [];

  constructor(private blogService: BlogService) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.isVisible = false;
    this.blogService.getMyBlogs().subscribe((data) => {
      this.items = data;
    });
  }

  addBlog(title: string, text: string) {
    this.blogService.addBlog(title, text)
      .subscribe(() => {
        this.init();
      });
  }

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }

  deleteBlog(id: Guid) {
    this.blogService.deleteBlog(id).subscribe(() => {
      this.init();
    });
  }

  editBlog(blogId: Guid, title: string, text: string) {
    this.blogService.editBlog(blogId, title, text).subscribe(() => {
      this.init();
    });
  }
}
