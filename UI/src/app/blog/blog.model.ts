import { Guid } from 'guid-typescript';

export class Blog {
  id: Guid;
  title: string;
  text: string;
  isMy: boolean;
}
