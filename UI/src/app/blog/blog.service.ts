import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Guid } from 'guid-typescript';
import { Observable } from 'rxjs';
import { Blog } from './blog.model';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient) { }

  getBlogs(): Observable<Blog[]> {
    return this.http.get<Blog[]>('/api/blog/GetAll');
  }

  getMyBlogs(): Observable<Blog[]> {
    return this.http.get<Blog[]>('/api/blog/GetAllUserBlogs');
  }

  getBlogBy(blogId: Guid): Observable<Blog> {
    return this.http.get<Blog>(`/api/blog/Get/${blogId}`);
  }

  addBlog(title: string, text: string): Observable<string> {
    const model = { title, text };
    return this.http.post<string>('/api/blog/AddBlog', model);
  }

  deleteBlog(id: Guid): Observable<Blog> {
    return this.http.delete<Blog>('/api/blog/Delete/' + id);
  }

  editBlog(blogId: Guid, title: string, text: string): Observable<string> {
    const model = { blogId: blogId.toString(), title, text};
    return this.http.put<string>('/api/blog/Update', model);
  }
}
