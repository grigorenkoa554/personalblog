import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Guid } from 'guid-typescript';
import { Blog } from '../blog.model';
import { BlogService } from '../blog.service';

@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  styleUrls: ['./blog-details.component.less']
})
export class BlogDetailsComponent implements OnInit {
  blogId: Guid;
  blog: Blog;
  constructor(private blogService: BlogService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.init();
  }

  init() {
    this.route.params.subscribe(params => {
      const blogIdString = params['blogId'];
      this.blogId = Guid.parse(blogIdString);
      this.blogService.getBlogBy(this.blogId)
        .subscribe((x) => {
          this.blog = x;
        });
    });
  }

}
