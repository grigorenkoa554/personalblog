import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LoginResult } from './account.model';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  nickname: string;

  constructor(private http: HttpClient) { }

  getNickname(): string {
    return this.nickname;
  }

  register(email: string, password: string, checkPassword: number, nickname: string):
    Observable<string> {
    const model = { email, password, checkPassword, nickname };
    return this.http.post<string>('/api/Account/Register', model);
  }

  login(email: string, password: string): Observable<LoginResult> {
    const model = { email, password };
    return this.http.post<LoginResult>('/api/Account/Login', model).pipe(map(data => {
      this.nickname = data.nickname;
      return data;
    }));
  }

  logout(): Observable<string> {
    const model = {};
    return this.http.post<string>('/api/Account/Logout', model).pipe(map(data => {
      this.nickname = '';
      return data;
    }));
  }
}
