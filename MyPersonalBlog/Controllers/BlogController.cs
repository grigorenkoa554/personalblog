﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyPersonalBlog.Models;
using MyPersonalBlog.Services;
using System;
using System.Collections.Generic;

namespace MyPersonalBlog.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class BlogController : ControllerBase
	{
		private readonly IBlogService blogService;
		public BlogController(IBlogService blogService)
		{
			this.blogService = blogService;
		}

		[HttpGet("[action]/{id}")]
		public Blog Get(Guid id)
		{
			return blogService.GetBlogById(id);
		}

		[HttpGet("[action]")]
		public IEnumerable<Blog> GetAll()
		{
			return blogService.GetAllBlogs();
		}

		[HttpDelete("[action]/{id}")]
		public void Delete(Guid id)
		{
			blogService.DeleteBlog(id);
		}

		[HttpGet("[action]")]
		public IEnumerable<Blog> GetAllUserBlogs()
		{
			return blogService.GetAllUserBlogs();
		}

		[Authorize]
		[HttpPost("[action]")]
		public object AddBlog(AddBlogModel blog)
		{
			blogService.AddBlog(blog);
			return new { res = "Added." };
		}

		[Authorize]
		[HttpPut("[action]")]
		public object Update(UpdateBlogModel blog)
		{
			blogService.UpdateUserBlog(blog);
			return new { res = "Updated." };
		}
	}
}
