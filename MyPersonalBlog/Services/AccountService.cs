﻿using Microsoft.AspNetCore.Identity;
using MyPersonalBlog.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace MyPersonalBlog.Services
{
	public interface IAccountService
	{
		Task<object> Register(RegisterModel model);
		Task<string> Login(LoginModel model);
		Task<object> Logout();
	}

	public class AccountService : IAccountService
	{
		private readonly UserManager<User> userManager;
		private readonly SignInManager<User> signInManager;
		public AccountService(UserManager<User> userManager, SignInManager<User> signInManager)
		{
			this.userManager = userManager;
			this.signInManager = signInManager;

		}
		public async Task<object> Register(RegisterModel model)
		{
			User user = new User
			{
				Email = model.Email,
				UserName = model.Email,
				Created = DateTime.Now,
				Nickname = model.Nickname
			};

			var result = await userManager.CreateAsync(user, model.Password);

			if (result.Succeeded)
			{
				await userManager.AddToRoleAsync(user, "User");
			}
			else
			{
				throw new ArgumentException("Failed register.");
			}

			return new { res = "ok" };
		}

		public async Task<string> Login(LoginModel model)
		{
			var user = await userManager.FindByEmailAsync(model.Email);

			if (user != null)
			{
				var result = await signInManager
					.PasswordSignInAsync(model.Email, model.Password, false, false);

				if (result.Succeeded)
				{
					return user.Nickname;
				}
				else
				{
					throw new ArgumentException();
				}
			}
			else
			{
				throw new ArgumentException();
			}
		}

		public async Task<object> Logout()
		{
			await signInManager.SignOutAsync();

			return new { res = "ok" };
		}
	}

	public class RegisterModel
	{
		[Required]
		public string Email { get; set; }
		[Required]
		public string Password { get; set; }
		[Required]
		public string CheckPassword { get; set; }
		public string Nickname { get; set; }
	}

	public class LoginModel
	{
		[Required]
		public string Email { get; set; }

		[Required]
		public string Password { get; set; }
	}
}
