﻿using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;

namespace MyPersonalBlog.Services
{
    public interface IAuthService
    {
        Guid CurrentUserId { get; }
    }

    public class AuthService : IAuthService
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private Guid currentUserId;

        public AuthService(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public Guid CurrentUserId
        {
            get
            {
                if (currentUserId == default)
                {
                    var str = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                    Guid.TryParse(str, out currentUserId);
                }
                return currentUserId;
            }
        }
    }
}
