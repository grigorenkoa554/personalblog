﻿using MyPersonalBlog.Models;
using MyPersonalBlog.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MyPersonalBlog.Services
{
	public interface IBlogService
	{
		void AddBlog(AddBlogModel model);
		void DeleteBlog(Guid id);
		IEnumerable<Blog> GetAllUserBlogs();
		IEnumerable<Blog> GetAllBlogs();
		Blog GetBlogById(Guid id);
		object UpdateUserBlog(UpdateBlogModel blog);
	}

	public class BlogService : IBlogService
	{
		private readonly IAuthService authService;
		private readonly IBaseRepository<Blog> blogRepository;
		public BlogService(IAuthService authService,
			IBaseRepository<Blog> blogRepository)
		{
			this.authService = authService;
			this.blogRepository = blogRepository;
		}

		public void AddBlog(AddBlogModel model)
		{
			var blog = new Blog
			{
				Text = model.Text,
				Title = model.Title,
				UserId = authService.CurrentUserId
			};
			using (blogRepository)
			{
				blogRepository.Add(blog);
			}
		}

		public void DeleteBlog(Guid id)
		{
			using (blogRepository)
			{
				blogRepository.Delete(id);
			}
		}

		public IEnumerable<Blog> GetAllBlogs()
		{
			var blogs = blogRepository.GetAll();
			return blogs;
		}

		public IEnumerable<Blog> GetAllUserBlogs()
		{
			var allBlogs = blogRepository.GetAll(x => x.UserId == authService.CurrentUserId);
			return allBlogs;
		}

		public Blog GetBlogById(Guid id)
		{
			var blog = blogRepository.GetById(id);
			return blog;
		}

		public object UpdateUserBlog(UpdateBlogModel model)
		{
			var existsBlog = blogRepository
				.GetAll(x => x.UserId == authService.CurrentUserId)
				.SingleOrDefault(x => x.Id == model.BlogId);
			if (existsBlog == null)
			{
				throw new ArgumentException($"Blog with id: {model.BlogId} not exists.");
			}
			existsBlog.Text = model.Text;
			existsBlog.Title = model.Title;
			blogRepository.Update(existsBlog);

			return new { res = "Youre blog is update!" };
		}
	}

	public class AddBlogModel
	{
		[Required]
		public string Title { get; set; }
		[Required]
		public string Text { get; set; }
	}

	public class UpdateBlogModel
	{
		[Required]
		public Guid BlogId { get; set; }
		[Required]
		public string Title { get; set; }
		[Required]
		public string Text { get; set; }
	}
}