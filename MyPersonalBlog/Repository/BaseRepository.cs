﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyPersonalBlog.Controllers;
using MyPersonalBlog.FirstDbContext;
using MyPersonalBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyPersonalBlog.Repository
{
    public interface IBaseRepository<T> : IDisposable
    {
        void Add(T item);
        List<T> GetAll();
        List<T> GetAll(Func<T, bool> func);
        void Delete(Guid id);
        T GetById(Guid id);
        void Update(T item);

    }
    public class BaseRepository<T> : IBaseRepository<T>
        where T : BaseEntity, new()
    {
        protected readonly DbSet<T> table;
        private readonly MyDbContext dbContext;

        public BaseRepository(MyDbContext dbContext)
        {
            table = dbContext.Set<T>();
            this.dbContext = dbContext;
        }
        public void Add(T item)
        {
            table.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(Guid id)
        {
            T item = table.First(x => x.Id == id);
            table.Remove(item);
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            dbContext?.Dispose();
        }

        public List<T> GetAll()
        {
            return table.ToList();
        }        
        
        public List<T> GetAll(Func<T, bool> condition)
        {
            return table.Where(condition).ToList();
        }

        public T GetById(Guid id)
        {
            return table.First(x => x.Id == id);
        }

        public void Update(T item)
        {
            if (item.Id == default)
            {
                throw new ArgumentException($"Id not set. It is '0'. Type of entity: {typeof(T)}");
            }
            table.Update(item);
            dbContext.SaveChanges();
        }
    }
}
