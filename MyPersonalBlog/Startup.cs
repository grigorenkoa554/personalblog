﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using MyPersonalBlog.FirstDbContext;
using MyPersonalBlog.Models;
using MyPersonalBlog.Repository;
using MyPersonalBlog.Services;
using System;
using System.IO;
using System.Security.Claims;

namespace MyPersonalBlog
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			// session
			services.AddDistributedMemoryCache();
			services.AddSession();

			// context
			var connection = Configuration.GetConnectionString("DefaultConnection");
			services.AddDbContext<MyDbContext>(options => options.UseSqlServer(connection));

			services.AddIdentity<User, IdentityRole>(
					options =>
					{
						options.Password.RequireLowercase = false;
						options.Password.RequireUppercase = false;
						options.Password.RequireDigit = false;
						options.Password.RequiredLength = 1;
						options.Password.RequireNonAlphanumeric = false;
					})
				.AddEntityFrameworkStores<MyDbContext>()
				.AddDefaultTokenProviders();

			services.AddScoped<IBaseRepository<Blog>, BaseRepository<Blog>>();
			services.AddScoped<IAuthService, AuthService>();
			services.AddScoped<IBlogService, BlogService>();
			services.AddScoped<IAccountService, AccountService>();

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "MyPersonalBlog", Version = "v1" });
			});

			services.AddControllers();
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseSwagger();
				app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MyPersonalBlog v1"));
			}

			app.Use(async (context, next) =>
			{
				await next();
				if (context.Response.StatusCode == 404 &&
				   !Path.HasExtension(context.Request.Path.Value) &&
				   !context.Request.Path.Value.StartsWith("/api/"))
				{
					context.Request.Path = "/index.html";
					await next();
				}
			});
			app.UseDefaultFiles();
			app.UseStaticFiles();

			app.UseSession();

			app.UseHttpsRedirection();

			app.UseRouting();

			app.UseAuthentication();
			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
