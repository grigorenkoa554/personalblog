﻿using System;

namespace MyPersonalBlog.Models
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
