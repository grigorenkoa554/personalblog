﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyPersonalBlog.Models
{
    public class UserResource : BaseEntity
    {
        public Guid UserId { get; set; }
    }
}
