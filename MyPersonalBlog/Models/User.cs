﻿using Microsoft.AspNetCore.Identity;
using System;

namespace MyPersonalBlog.Models
{
    public class User : IdentityUser
    {
        public DateTime Created { get; set; }
        public string Nickname { get; set; }
    }
}
