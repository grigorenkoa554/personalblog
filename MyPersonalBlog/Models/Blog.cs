﻿namespace MyPersonalBlog.Models
{
    public class Blog : UserResource
    {
        public string Title { get; set; }
        public string Text { get; set; }
    }
}
